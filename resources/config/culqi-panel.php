<?php
return [
    /**
     * Support 'production' or 'test'
     */
    'mode' => 'production',
    'tokens' => [
        'test' => env('CULQI_TEST_TOKEN'),
        'production' => env('CULQI_TOKEN'),
    ],
    'views' => [
        'path' => __DIR__.'/../views',
        'layout' => 'layouts.master'
    ]
];