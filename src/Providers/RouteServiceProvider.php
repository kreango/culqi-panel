<?php

namespace Kreango\CulqiPanel\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    protected $namespace = 'Kreango\CulqiPanel\Http\Controllers';

    public function boot()
    {
        parent::boot();
    }

    public function map()
    {
        Route::middleware('auth')
            ->prefix('culqi')
            ->as('culqi')
            ->namespace($this->namespace)
            ->group(function() {
                Route::get('/{object}', ['uses' => 'ViewController@index', 'as' => 'index']);
                Route::get('/{object}/create', ['uses' => 'ViewController@create', 'as' => 'create']);
                Route::get('/{object}/{id}/edit', ['uses' => 'ViewController@edit', 'as' => 'edit']);
            });

        Route::middleware('api')
            ->prefix('api/culqi')
            ->as('culqi-api')
            ->namespace($this->namespace)
            ->group(function() {
                Route::group([
                    'prefix' => 'tokens',
                    'as' => 'tokens.'
                ], function() {
                    Route::get('/', ['uses' => 'TokenController@index', 'as' => 'index']);
                    Route::get('/{id}', ['uses' => 'TokenController@show', 'as' => 'show']);
                    Route::post('/', ['uses' => 'TokenController@store', 'as' => 'store']);
                    Route::put('/{id}', ['uses' => 'TokenController@update', 'as' => 'update']);
                });

                Route::group([
                    'prefix' => 'charges',
                    'as' => 'charges.'
                ], function() {
                    Route::get('/', ['uses' => 'ChargeController@index', 'as' => 'index']);
                    Route::get('/{id}', ['uses' => 'ChargeController@show', 'as' => 'show']);
                    Route::post('/', ['uses' => 'ChargeController@store', 'as' => 'store']);
                    Route::put('/{id}', ['uses' => 'ChargeController@update', 'as' => 'update']);
                    Route::post('/{id}/capture', ['uses' => 'ChargeController@capture', 'as' => 'capture']);
                });

                Route::group([
                    'prefix' => 'customers',
                    'as' => 'customers.'
                ], function() {
                    Route::get('/', ['uses' => 'CustomerController@index', 'as' => 'index']);
                    Route::get('/{id}', ['uses' => 'CustomerController@show', 'as' => 'show']);
                    Route::post('/', ['uses' => 'CustomerController@store', 'as' => 'store']);
                    Route::put('/{id}', ['uses' => 'CustomerController@update', 'as' => 'update']);
                    Route::delete('/{id}', ['uses' => 'CustomerController@destroy', 'as' => 'destroy']);
                });

                Route::group([
                    'prefix' => 'cards',
                    'as' => 'cards.'
                ], function() {
                    Route::get('/', ['uses' => 'CardController@index', 'as' => 'index']);
                    Route::get('/{id}', ['uses' => 'CardController@show', 'as' => 'show']);
                    Route::post('/', ['uses' => 'CardController@store', 'as' => 'store']);
                    Route::put('/{id}', ['uses' => 'CardController@update', 'as' => 'update']);
                    Route::delete('/{id}', ['uses' => 'CardController@destroy', 'as' => 'destroy']);
                });

                Route::group([
                    'prefix' => 'plans',
                    'as' => 'plans.'
                ], function() {
                    Route::get('/', ['uses' => 'PlanController@index', 'as' => 'index']);
                    Route::get('/{id}', ['uses' => 'PlanController@show', 'as' => 'show']);
                    Route::post('/', ['uses' => 'PlanController@store', 'as' => 'store']);
                    Route::put('/{id}', ['uses' => 'PlanController@update', 'as' => 'update']);
                    Route::delete('/{id}', ['uses' => 'PlanController@destroy', 'as' => 'destroy']);
                });

                Route::group([
                    'prefix' => 'refunds',
                    'as' => 'refunds.'
                ], function() {
                    Route::get('/', ['uses' => 'RefundController@index', 'as' => 'index']);
                    Route::get('/{id}', ['uses' => 'RefundController@show', 'as' => 'show']);
                    Route::post('/', ['uses' => 'RefundController@store', 'as' => 'store']);
                    Route::put('/{id}', ['uses' => 'RefundController@update', 'as' => 'update']);
                });
            });
    }
}