<?php

namespace Kreango\CulqiPanel;

use Culqi\Culqi as BaseCulqi;

class Culqi
{
    protected $mode;

    protected $token;

    protected $culqi;

    public function __construct()
    {
        $this->mode = config('culqi-panel.mode');
        $this->token = config('culqi-panel.tokens.'.$this->mode);
        $this->culqi = new BaseCulqi(['api_token' => $this->token]);
    }

    /**
     * Environment mode 'test' or 'production'
     *
     * @return string
     */
    public function getMode() : string
    {
        return $this->mode;
    }

    /**
     * Get current Api Token
     *
     * @return string
     */
    public function getToken() : string
    {
        return $this->token;
    }

    /**
     * Get instance culqi
     *
     * @return BaseCulqi
     */
    public function getInstance() : BaseCulqi
    {
        return $this->culqi;
    }
}