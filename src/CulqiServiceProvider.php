<?php

namespace Kreango\CulqiPanel;

use Culqi\Culqi;
use Illuminate\Support\ServiceProvider;
use Kreango\CulqiPanel\Providers\RouteServiceProvider;

class CulqiServiceProvider extends ServiceProvider
{
    public function boot() 
    {
        $this->mergeConfigFrom(__DIR__.'/../resources/config/culqi-panel.php', 'culqi-panel');
    }

    public function register() 
    {
        $this->app->register(RouteServiceProvider::class);
        $this->app->alias(Culqi::class, Facade::class);
    }
}